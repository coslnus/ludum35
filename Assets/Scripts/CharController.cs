﻿using UnityEngine;
using System.Collections;

public class CharController : MonoBehaviour {

	private Animator animator;
	private Rigidbody rigidbody;

	private float maxSpeed = 15f;
	private float moveForce = 100f;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown (KeyCode.Space)) {
//			animator.SetTrigger("kick");
//		}
			
		float h = Input.GetAxisRaw("Horizontal");

		// Анимация
		if (Mathf.Abs(h) > 0) {
			animator.SetBool ("IsRunning", true);
		} else {
			animator.SetBool ("IsRunning", false);
		}

		// Сила действующая на движение
		if (h * rigidbody.velocity.x < maxSpeed) {
			rigidbody.AddForce(Vector2.right * h * moveForce);
		}

		// Ограничение скорости
		if (Mathf.Abs (rigidbody.velocity.x) > maxSpeed) {
			rigidbody.velocity = new Vector2(Mathf.Sign(rigidbody.velocity.x) * maxSpeed, rigidbody.velocity.y);
		}
	}

	void FixedUpdate() {
		// чтобы на земле сразу останавливался, а в прыжке продолжал двигаться по инерции оси х
		if (Input.GetAxisRaw("Horizontal") == 0) {
			rigidbody.velocity = new Vector2(0f, rigidbody.velocity.y);
		}
	}
}
